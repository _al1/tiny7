/*
Copyright (c) 2015, al1
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

1.	Redistributions of source code must retain the above copyright notice, this list of conditions 
	and the following disclaimer.
2.	Redistributions in binary form must reproduce the above copyright notice, this list of 
	conditions and the following disclaimer in the documentation and/or other materials provided 
	with the distribution.
3. 	Neither the name of the copyright holder nor the names of its contributors may be used to 
	endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Arduino.h"
#include "tiny7.h"
#include <stdarg.h>

#define seg0	 0b11111100U // 0
#define seg1	 0b01100000U // 1
#define seg2	 0b11011010U // 2
#define seg3	 0b11110010U // 3
#define seg4	 0b01100110U // 4
#define seg5	 0b10110110U // 5
#define seg6	 0b10111110U // 6
#define seg7	 0b11100000U // 7
#define seg8	 0b11111110U // 8
#define seg9	 0b11110110U // 9	
#define segM	 0b00000010U // 10
#define segA	 0b11101110U // A 11
#define segB	 0b00111110U // b 12
#define segC	 0b10011100U // C 13
#define segD	 0b01111010U // d 14
#define segE	 0b10011110U // E 15
#define segF	 0b10001110U // F 16
#define segN	 0b00101010U // n 17
#define segT	 0b00011110U // t 18
#define segI	 0b00100000U // i 19
#define segY	 0b01110110U // y 20
#define segEmpty 0 			 // 21
#define segCount 22

static  uint8_t seg_char[segCount] = {seg0, seg1, seg2, 
	seg3, seg4, seg5, seg6, seg7, seg8, seg9, segA, segB, 
	segC, segD, segE, segF, segN, segM, segT, segI, segY, 
	segEmpty};
	
#define UNPRINT 0
static uint8_t ASCII2SevenSeg[0x7F]={

	UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, 	// 0x00-0x07
	UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, 	// 0x08-0x0F
	UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, 	// 0x10-0x17
	UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, UNPRINT, 	// 0x18-0x1F
	0,			//   (space) ---------------------------------------------------// 0x20
	UNPRINT,	// !
	0b01000100, // "
	UNPRINT, UNPRINT, UNPRINT, UNPRINT, // #,$,%.&
	0b01000000, // '
	0b10011100, // (
	0b11110000, // )
	UNPRINT,    // *
	0b01100010, // +
	UNPRINT, 	// ,
	0b00000010, // -
	UNPRINT,    // .
	0b01001010, // /
	0b11111100, // 0 ------------------------------------------------------------// 0x30
	0b01100000, // 1
	0b11011010, // 2
	0b11110010, // 3
	0b01100110, // 4
	0b10110110, // 5
	0b10111110, // 6
	0b11100000, // 7
	0b11111110, // 8
	0b11110110, // 9	
	UNPRINT,  UNPRINT,  UNPRINT,  //:,;,<
	0b00010010, // =
	UNPRINT,  	// >
	0b01001110, // ? looks more like µ
	0b11110110, // @ ------------------------------------------------------------// 0x40
	0b11101110,	// A		capital letters
	0b11111110, // B
	0b10011100,	// C
	0b11111000, // D
	0b10011110, // E
	0b10001110, // F / f
	0b10111100, // G
	0b01101110, // H
	0b01100000, // I
	0b11111000, // J
	0b10101110, // k
	0b00011100, // L
	0b10101000, // M
	0b11101100, // N
	0b11111100, // O	
	0b11001110, // P /p
	0b11010110, // Q
	0b11001100, // R	
	0b10110110, // S /s
	0b11100000, // T
	0b01111100, // U
	0b01001110, // V
	0b01111110, // W
	0b00100110, // X/x
	0b01110110, // Y/y
	0b11011010, // Z/z
	0b10011100, // [
	0b00100110, // backslash
	0b11110000, // ]
	0b11000100, // ^
	0b00010000,  // _ (underline)
	0b11000110, // ° (degree instead of ` ->see ') ---------------------------------//  0x60
	0b11111010, // a		small letters
	0b00111110,	// b
	0b00011010, // c
	0b01111000,	// d
	0b11011110, // e
	0b10001110, // F / f
	0b11110110, // g
	0b00101110, // h
	0b00100000, // i
	0b01110000, // j
	0b10101110, // k
	0b00001100, // l
	0b00101000, // m
	0b00101010, // n
	0b00111010, // o
	0b11001110, // P /p
	0b11100110, // q
	0b00001010, // r
	0b10110110, // S /s
	0b00011110, // t
	0b00111000, // u
	0b01000110, // v
	0b01010100, // w
	0b00100110, // X/x
	0b01110110, // Y/y
	0b11011010, // Z/z
	0b10011100, // {
	0b00001100, // |
	0b11110000, // }
	0b00100010 // ~ can be used as logical not 
	//UNPRINT //----------------------------------------------------------------------// 0x7F
};	

uint8_t invertSeg_char(uint8_t inChar)
{
	uint8_t invertChar;

	invertChar  = (inChar>>5) & 0x06 ;	// a b
	invertChar |= (inChar<<5) & 0xC0 ;	// f g
	invertChar |= (inChar<<3) & 0x08 ;	// DP
	invertChar |= (inChar>>3) & 0x01 ;	// e
	invertChar |= (inChar<<1) & 0x20 ;	// d
	invertChar |= (inChar>>1) & 0x10 ;	// c

	return invertChar;
}

uint8_t rotSeg_char(uint8_t in)
{
	uint8_t ret=0;
	
	ret |= (in>>3) & 0x1C;	// a b c
	ret |= (in<<3) & 0xE0;	// d e f
	ret |= (in<<1) & 0x02;	// DP
	ret |= (in>>1) & 0x01;  // g 
	
	return ret;
}

tiny7::tiny7()	// constructor
{
	
}

void tiny7::begin(char inLength, char inInvert, char commonPin, char inSDO, char inSCK, char inLatch)	
{
	if (inSDO==127)	// 127 == default value -> no user settigne
	{
		#if (defined(__AVR_ATtiny45__))|| (defined(__AVR_ATtiny85__))	// but some forgot so some non void values (mapped to ATtiny45/85)
			inSDO=0;
			inSCK=2;
			inLatch=1;
		#elif (defined(__AVR_ATmega328P__))||(defined(__AVR_ATmega2560__))||(defined(__AVR_ATmega32u4__)) // default values for most Arduinos are stored in defined MISO, SCK and MISO
			inSDO=MOSI;
			inSCK=SCK;
			inLatch=MISO;
		#else
			// nothing in here
		#endif
	}
	_sdo_pin=inSDO;
	_clk_pin=inSCK;
	_latch_pin=inLatch;
	_length=inLength;
		
	pinMode(_sdo_pin, OUTPUT);
	pinMode(_clk_pin, OUTPUT);
	pinMode(_latch_pin, OUTPUT);
	
	if ((inInvert==1)||(inInvert==3))	//1 only mirror 3 mirror and rotate
	{
		for(char i=0; i<0x7F; i++)
			ASCII2SevenSeg[i]=invertSeg_char(ASCII2SevenSeg[i]);
		_DPpos=3;
	}
	
	if ((inInvert==2)||(inInvert==3)) //2 only rot 3 mirror and rotate
	{
		for(char i=0; i<0x7F; i++)
			ASCII2SevenSeg[i]=rotSeg_char(ASCII2SevenSeg[i]);
		_DPpos=1;
		if (inInvert==3) _DPpos=6;
	}
	
	if (commonPin==T7_CA) // if common Anode mode
	{
		for(char i=0; i<0x7F; i++)
			ASCII2SevenSeg[i]=~(ASCII2SevenSeg[i]); //invert all chars 
	}
}

void tiny7::show(char inVar, char dot)
{
	char tempChar = seg_char[inVar];
	if (dot)
	{
	tempChar|=(1<<_DPpos);
	/*	if(_invert)
			tempChar+=8;
		else
			tempChar+=1;*/
	}
	out(tempChar);
}

void tiny7::out(char var)
{
	shiftOut(_sdo_pin, _clk_pin,LSBFIRST , var);
	digitalWrite(_latch_pin, HIGH);
	digitalWrite(_latch_pin, LOW);
}

void tiny7::outBuffer(char *buffer)
{
	
	for (char i =_length-1; i>=0; i--)
	{
		shiftOut(_sdo_pin, _clk_pin,LSBFIRST , buffer[i]);
	}
	digitalWrite(_latch_pin, HIGH);
	digitalWrite(_latch_pin, LOW);
}

size_t tiny7::write(uint8_t character)
{
	this->out(ASCII2SevenSeg[character]);
}

size_t tiny7::write(const char *str)
{
	char buffer[_length];
	char end=0;
	for (char i=0; i<_length; i++)
	{
		if (!end)
			buffer[i]=ASCII2SevenSeg[(*str)];
		else
			buffer[i]=0;
		str++;
		if (!(*str))
			end=1;
	}
	outBuffer(buffer);
	
}

size_t tiny7::write(const uint8_t *buffer, size_t size)
{
	char tempBuffer[_length];
	for (char i=0; i<_length; i++)
	{
		if (i<size)
			tempBuffer[i]=ASCII2SevenSeg[(buffer[i])];
		else
			tempBuffer[i]=0;
	}
	outBuffer(tempBuffer);
}

void tiny7::test()
{
;

}

void tiny7::printf(char *fmt, ... ){	// see: http://playground.arduino.cc/Main/Printf
        char buf[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 128, fmt, args);
        va_end (args);
        this->print(buf);
}
