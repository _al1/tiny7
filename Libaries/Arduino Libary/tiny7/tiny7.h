/*
Copyright (c) 2015, al1
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

1.	Redistributions of source code must retain the above copyright notice, this list of conditions 
	and the following disclaimer.
2.	Redistributions in binary form must reproduce the above copyright notice, this list of 
	conditions and the following disclaimer in the documentation and/or other materials provided 
	with the distribution.
3. 	Neither the name of the copyright holder nor the names of its contributors may be used to 
	endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


/*
helpfull website: http://playground.arduino.cc/Code/Printclass

*/

#ifndef tiny7_h
	#define tiny7_h
	#include "Arduino.h"
	#include <Print.h>
	#define T7_NORMAL 0
	#define T7_MIRROR 1		//1 only mirror 3 mirrow and rotate
	#define T7_ROT	  2 //2 only rot 3 mirrow and rotate
	#define T7_ROTMIR 3
	
	#define T7_CC 0	//common cathode
	#define T7_CA 1 //common anode
	
	class tiny7 : public Print
	{
		public:
			tiny7();
			void begin(char inLength=1, char inInvert=0, char commonPin= T7_CC, char inSDO=127, char inSCK=127, char inLatch=127);
			void show(char inVar, char dot=0);
			void test();
			virtual size_t write(uint8_t);
			virtual size_t write(const char *str);
			virtual size_t write(const uint8_t *buffer, size_t size);
			void printf(char *fmt, ... );
		private:
			char _sdo_pin;
			char _latch_pin;
			char _clk_pin;
			char _invert;
			char _DPpos=0;
			char _length=1;
			void out(char var);
			void outBuffer(char *buffer);
	};
	
#endif